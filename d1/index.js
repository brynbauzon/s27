//Importing http library
const http = require('http');

//Mock Database Mimicing to POST and GET into Postman

let directory = [

	{

		"name"	: "Brandon",
		"email"	: "brandon@gmail.com"
	},
	{
		"name"	: "Jobert",
		"email"	: "jobert@gmail.com"
	}

]


//Creating server request and response
const server = http.createServer((req, res)=>{

	if(req.url == "/users" && req.method == "GET"){

		 res.writeHead(200, {'Content-Type': 'text/plain'});
		// res.end('<p>Data retrieved from database</p>');

		res.write(JSON.stringify(directory));
		res.end('Data retrieved from the database');


	}

    //Add New User
		if(req.url == "/users" && req.method == "POST"){

		// res.writeHead(200, {'Content-Type': 'text/plain'});
		// res.end('<p>Data retrieved from database</p>');


		let requestBody = '';

		req.on('data', (data)=>{
			requestBody += data;

		});


		req.on('end', ()=>{

			// Check if at this point the requestBody is a data type of STRING
			// We need this to be of data type JSON to access its properties
			console.log(typeof requestBody);
			console.log(requestBody);

			//Converts the string requestBody to JSON
			requestBody = JSON.parse(requestBody);

			//Create a new object representing the new mock database record
			let newUser = {

				"name": requestBody.name,
				"email": requestBody.email

			}

			directory.push(newUser)
			console.log(directory)

			res.writeHead(200, {'Content-Type': 'application/json'});
			res.write(JSON.stringify(newUser));
			res.end();


		});


	}

});



//Listening for port 3000
server.listen(3000, 'localhost', ()=>{

 console.log('Listening to port 3000')

});

